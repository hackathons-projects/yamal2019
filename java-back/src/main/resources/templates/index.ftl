<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Sidebar - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/simple-sidebar.css" rel="stylesheet">
    <link href="../css/main.css" rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" type="text/css" href="../vendor/datatables/css/jquery.dataTables.min.css"/>

    <script type="text/javascript" src="../vendor/datatables/js/jquery.dataTables.min.js"></script>


    <link rel="stylesheet" type="text/css" media="screen" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    
    
    <script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    


</head>

<body>

<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
        <div class="sidebar-heading">Барбосиум</div>
        <div class="list-group list-group-flush">
            <a href="#" class="list-group-item list-group-item-action bg-light" onclick="enableContent('animals')">
                Список животных</a>
            <a href="#" class="list-group-item list-group-item-action bg-light" onclick="enableContent('profile')">
                Мой профиль</a>
            <a href="#" class="list-group-item list-group-item-action bg-light" onclick="enableContent('schedule')">
                Мое расписание</a>
            <a href="#" class="list-group-item list-group-item-action bg-light"
               onclick="enableContent('notifications')">
                Уведомления</a>
            <a href="#" class="list-group-item list-group-item-action bg-light" onclick="enableContent('documents')">
                Документы</a>
            <a href="#" class="list-group-item list-group-item-action bg-light" onclick="enableContent('reports')">
                Отчеты</a>
        </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <button class="btn btn-primary" id="menu-toggle">Toggle Menu</button>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            ${userName}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" onclick="enableContent('profile')">Мой профиль</a>
                            <a class="dropdown-item" onclick="enableContent('schedule')">Мое расписание</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/logout">Выход</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>

        <#--      CONTENT-->
        <div id="animals" class="container-fluid">
            <table id="animalTable" class="display">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Кличка животного</th>
                    <th>Пол</th>
                    <th>Вид животного</th>
                    <th>Дата поступления</th>
                    <th>Дата рождения</th>
                    <th>Статус</th>
                    <th>Обработка от глистов</th>
                    <th>Стериилизация</th>
                    <th>Дата стериилизации</th>
                    <th>Описание</th>
                    <th>Владелец</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <div id="animal" class="container-fluid disabled-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 ">
                        <form class="form-horizontal">
                            <!-- Заголовок -->
                            <legend>Карточка животного</legend>
                            <!-- Кличка-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Кличка животного</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user">
                                            </i>
                                        </div>
                                        <input id="animalName" name="name" type="text"
                                               placeholder="Кличка животного" class="form-control input-md">
                                    </div>
                                </div>
                            </div>
                            <!-- Загрузить фото -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Upload photo">Загрузить фото</label>
                                <div class="col-md-4">
                                    <input id="Upload photo" name="Upload photo" class="input-file" type="file">
                                </div>
                            </div>
                            <!-- Дата рождения-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="birthday">Дата рождения</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-birthday-cake"></i>
                                        </div>
                                        <input id="animalBirthday" name="birthday" type="text"
                                               placeholder="Дата рождения" class="form-control input-md">
                                    </div>
                                </div>
                            </div>
                            <!-- Дата поступления в приют-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="receiveDate">Дата поступления в
                                    приют</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-birthday-cake"></i>
                                        </div>
                                        <input id="receiveDate" name="receiveDate" type="text"
                                               placeholder="Дата поступления в приют" class="form-control input-md">
                                    </div>
                                </div>
                            </div>
                            <!-- Пол животного -->
                            <fieldset id="genderGroup">
                                <div class="gender-group">
                                    <label class="col-md-4 control-label" for="gender">Пол животного:</label>
                                    <div class="col-md-4">
                                        <label class="radio-inline" for="gender-0">
                                            <input type="radio" name="gender" id="gender-0" value="1"
                                                   checked="checked">
                                            М
                                        </label>
                                        <label class="radio-inline" for="gender-1">
                                            <input type="radio" name="gender" id="gender-1" value="2">
                                            Ж
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            <!-- Стерилизация -->
                            <fieldset id="sterilizationGroup">
                                <div class="sterilization-group">
                                    <label class="col-md-4 control-label" for="radios">Стерилизация:</label>
                                    <div class="col-md-4">
                                        <label class="radio-inline" for="sterilization-0">
                                            <input type="radio" name="sterilization" id="sterilization-0" value="1"
                                                   checked="checked">
                                            Да
                                        </label>
                                        <label class="radio-inline" for="sterilization-1">
                                            <input type="radio" name="sterilization" id="sterilization-1" value="2"
                                                   checked="checked">
                                            Нет
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            <!-- Дата стерилизации-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="birthday">Дата стерилизации</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-birthday-cake"></i>
                                        </div>
                                        <input id="sterilizationDay" name="sterilization" type="text"
                                               placeholder="Дата стерилизации" class="form-control input-md">
                                    </div>
                                </div>
                            </div>
                            <!-- Обработка от глистов-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="birthday">Обработка от глистов</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-birthday-cake"></i>
                                        </div>
                                        <input id="sterilization" name="birthday" type="text"
                                               placeholder=" Обработка от глистов" class="form-control input-md">
                                    </div>
                                </div>
                            </div>
                            <!-- Тип животного -->
                            <fieldset id="animatypeGroup">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="radios">Тип животного:</label>
                                    <div class="col-md-4">
                                        <label class="radio-inline" for="radios-0">
                                            <input type="radio" name="animalType" id="animalType-0" value="1"
                                                   checked="checked">
                                            Кот
                                        </label>
                                        <label class="radio-inline" for="radios-1">
                                            <input type="radio" name="animalType" id="animalType-1" value="2">
                                            Пес
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            <!-- Статус животного -->
                            <fieldset id="fieldsetGroup">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="radios">Статус животного:</label>
                                    <div class="col-md-4">
                                        <label class="radio-inline" for="radios-0">
                                            <input type="radio" name="status" id="radios-0" value="1" checked="checked">
                                            В приюте
                                        </label>
                                        <br>
                                        <label class="radio-inline" for="radios-1">
                                            <input type="radio" name="status" id="status-1" value="2">
                                            Найден владелец
                                        </label>
                                        <br>
                                        <label class="radio-inline" for="radios-1">
                                            <input type="radio" name="status" id="status-2" value="3">
                                            Мертв
                                        </label>
                                        <br>
                                        <label class="radio-inline" for="radios-1">
                                            <input type="radio" name="status" id="status-3" value="4">
                                            Находится под присмотром (найден покровитель)
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            <!-- Описание -->
                            <div class="form-group">
                                <label class="col-md-4 control-label"
                                       for="Overview (max 200 words)">Описание</label>
                                <div class="col-md-4">
                                        <textarea class="form-control" rows="10" id="description"
                                                  name="description">Описание</textarea>
                                </div>
                            </div>
                            <!-- Владелец-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="owner">Владелец</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-user">
                                            </i>
                                        </div>
                                        <input id="owner" name="owner" type="text"
                                               placeholder="Владелец" class="form-control input-md">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-4">
                                    <a href="#" class="btn btn-success"><span
                                                class="glyphicon glyphicon-thumbs-up"></span> Сохранить</a>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-2 hidden-xs">
                        <img src="http://websamplenow.com/30/userprofile/images/avatar.jpg"
                             class="img-responsive img-thumbnail ">
                    </div>


                </div>
            </div>
        </div>

        <div id="profile" class="container-fluid disabled-content">
            <p>Профиль</p>
        </div>

        <div id="schedule" class="container-fluid disabled-content">
            <p>Личное расписание</p>
                <div id="time_container">
                <div id="datetimepicker11"></div>
                <div id="datetimepicker12"></div>
            </div>

            <ul id="intervals_list">

            </ul>
            
            <button onclick="addInterval()">Add Interval</button>
        </div>

        <div id="notifications" class="container-fluid disabled-content">
            Список оповещений и обращений
        </div>

        <div id="documents" class="container-fluid disabled-content">
            Полезные документы
        </div>

        <div id="reports" class="container-fluid disabled-content">
            Отчеты за период
        </div>

        <#--      CONTENT-->
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->


<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

    function enableContent(id) {
        $(".container-fluid").addClass("disabled-content");
        $("#" + id).removeClass("disabled-content");
    }

    function setAnimalData(data) {
        $("#animalName").val(data[1]);
        $("#receiveDate").val(data[4]);
        $("#animalBirthday").val(data[5]);
        $("#description").val(data[10]);
        $("#gender-0").prop('checked', data[2]);
        $("#gender-1").prop('checked', !data[2]);
        $("#sterilization-0").prop('checked', data[8]);
        $("#sterilization-1").prop('checked', !data[8]);

        switch (data[3]) {
            case 'CAT':
                $("#animalType-0").prop('checked', data[8]);
                $("#animalType-1").prop('checked', !data[8]);
                break;
            case  'DOG':
                $("#animalType-0").prop('checked', !data[8]);
                $("#animalType-1").prop('checked', data[8]);
                break;
        }

        switch (data[6]) {
            case 'IN_SHELTER':
                $("#status-0").prop('checked', data[6]);
                $("#status-1").prop('checked', !data[6]);
                $("#status-2").prop('checked', !data[6]);
                $("#status-3").prop('checked', !data[6]);
                break;
            case  'HAS_OWNER':
                $("#status-0").prop('checked', !data[6]);
                $("#status-1").prop('checked', data[6]);
                $("#status-2").prop('checked', !data[6]);
                $("#status-3").prop('checked', !data[6]);
                break;
            case  'DIED':
                $("#status-0").prop('checked', !data[6]);
                $("#status-1").prop('checked', !data[6]);
                $("#status-2").prop('checked', data[6]);
                $("#status-3").prop('checked', !data[6]);
                break;
            case  'WARD':
                $("#status-0").prop('checked', !data[6]);
                $("#status-1").prop('checked', !data[6]);
                $("#status-2").prop('checked', !data[6]);
                $("#status-3").prop('checked', data[6]);
                break;
        }

        $("#owner").val(data[11]);
    }

    $(document).ready(function () {
        var json_data = $.ajax({
                dataType: 'json', // Return JSON
                url: '/animals',
                type: "GET",
                "beforeSend": function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer ${jwt}");
                },
                success: function (data) {
                    var data_obj = [];
                    data.forEach(function (o) {
                        data_obj.push([
                            o.id,
                            o.name,
                            o.gender,
                            o.animalType,
                            o.receivedDate ? o.receivedDate[0] + '-' + o.receivedDate[1] + '-' + o.receivedDate[2] : "",
                            o.birthDate ? o.birthDate[0] + '-' + o.birthDate[1] + '-' + o.birthDate[2] : "",
                            o.animalStatus,
                            o.lastDeWormingDate ? o.lastDeWormingDate[0] + '-' + o.lastDeWormingDate[1] + '-' + o.lastDeWormingDate[2] : "",
                            o.sterilized,
                            o.sterilizationDate ? o.sterilizationDate[0] + '-' + o.sterilizationDate[1] + '-' + o.sterilizationDate[2] : "",
                            o.description,
                            o.ownerDto ? o.ownerDto.fio : ""

                        ]);
                    });

                    var table = $('#animalTable').dataTable({
                        data: data_obj,
                        columnDefs: [
                            {
                                render: function (data, type, row) {
                                    return data;
                                },
                                width: '5px',
                                targets: 0
                            },
                            {
                                render: function (data, type, row) {
                                    return data ? '<img src="https://img.icons8.com/color/48/000000/male.png">'
                                        : '<img src="https://img.icons8.com/color/48/000000/female.png">';
                                },
                                targets: 2
                            },
                            {
                                render: function (data, type, row) {
                                    switch (data) {
                                        case 'CAT':
                                            return '<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"\n' +
                                                'width="50" height="50"\n' +
                                                'viewBox="0 0 172 172"\n' +
                                                'style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-size="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g><path d="M167.02812,137.86875l-30.1,-133.43438c-0.40312,-2.01563 -2.28437,-3.35938 -4.43437,-3.09062c-0.94063,0.13438 -1.74687,0.5375 -2.28438,1.20938c-1.075,1.075 -3.49375,3.49375 -5.375,5.375c-3.89688,3.89688 -9.00313,6.45 -14.5125,6.71875c-6.31562,0.40313 -12.3625,-1.88125 -16.79687,-6.31562l-5.77812,-5.77812c-0.67187,-0.67187 -1.47812,-1.075 -2.28438,-1.20937c-2.15,-0.26875 -4.03125,1.20937 -4.43437,3.09062l-29.96563,133.43438c-0.80625,3.62812 -1.075,7.525 -0.40312,11.01875l-3.89688,1.74688l-4.43437,-3.62813l-20.29062,-4.70312c-4.3,-0.94063 -8.73437,0 -12.22812,2.82187c-3.62813,2.95625 -5.64375,7.39063 -5.375,12.09375c0.40312,7.65938 7.12187,13.57188 14.91563,13.57188h101.72188c1.075,0 2.01563,-0.26875 2.95625,-0.67187l4.03125,-2.01562l4.56875,2.6875h7.79375c8.0625,0 15.72187,-3.225 20.9625,-9.27188c5.375,-6.71875 7.525,-15.31875 5.64375,-23.65z" fill="#444b54"></path><path d="M45.82188,159.6375c0.67188,1.34375 2.15,2.15 3.62813,2.15c0.67188,0 1.20938,-0.13438 1.88125,-0.40312c2.01563,-1.075 2.82187,-3.49375 1.74687,-5.375c-1.20938,-2.28438 -2.01562,-4.70312 -2.41875,-7.12188l-8.33125,-1.88125c0.26875,4.43438 1.47812,8.6 3.49375,12.63125zM120.9375,170.65625h11.42188c0.40312,-1.20937 0.67188,-2.55312 0.67188,-4.03125c0,-5.24062 -3.35937,-9.675 -8.0625,-11.42187l-0.13437,-22.17187c0,-2.28438 -1.88125,-4.03125 -4.03125,-4.03125c-2.28438,0 -4.03125,1.88125 -4.03125,4.03125l0.13437,24.99375c0,0.13437 0,0.26875 0,0.5375c0,2.28438 1.74688,4.03125 4.03125,4.03125c2.28437,0 4.03125,1.74687 4.03125,4.03125c0,2.28438 -1.74688,4.03125 -4.03125,4.03125zM96.75,161.7875c-2.28437,0 -4.03125,-1.74687 -4.03125,-4.03125l-0.13437,-24.725c0,-2.28438 1.74687,-4.03125 4.03125,-4.03125v0c2.28437,0 4.03125,1.74687 4.03125,4.03125l0.13437,24.725c0,2.28438 -1.74688,4.03125 -4.03125,4.03125zM95.40625,41.65625c-2.2264,0 -4.03125,1.80485 -4.03125,4.03125c0,2.2264 1.80485,4.03125 4.03125,4.03125c2.2264,0 4.03125,-1.80485 4.03125,-4.03125c0,-2.2264 -1.80485,-4.03125 -4.03125,-4.03125zM122.28125,41.65625c-2.2264,0 -4.03125,1.80485 -4.03125,4.03125c0,2.2264 1.80485,4.03125 4.03125,4.03125c2.2264,0 4.03125,-1.80485 4.03125,-4.03125c0,-2.2264 -1.80485,-4.03125 -4.03125,-4.03125z" fill="#ffffff"></path></g></g></svg>';
                                        case  'DOG':
                                            return '<img src="https://img.icons8.com/ios-filled/50/000000/year-of-dog.png">';
                                    }
                                },
                                targets: 3
                            },
                            {
                                render: function (data, type, row) {
                                    switch (data) {
                                        case 'IN_SHELTER':
                                            return '<img src="https://img.icons8.com/material-sharp/50/000000/home.png">';
                                        case  'HAS_OWNER':
                                            return '<img src="https://img.icons8.com/ios-filled/50/000000/men-age-group-4.png">';
                                        case  'DIED':
                                            return '<img src="https://img.icons8.com/metro/52/000000/headstone.png">';
                                        case  'WARD':
                                            return '<img src="https://img.icons8.com/ios-filled/50/000000/dog-training.png">';
                                    }
                                },
                                targets: 6
                            },
                            {
                                render: function (data, type, row) {
                                    return data ? '<img src="https://img.icons8.com/material/48/000000/ok--v1.png">' : '<img src="https://img.icons8.com/color/48/000000/cancel--v1.png">'
                                },
                                targets: 7
                            },
                            {
                                render: function (data, type, row) {
                                    return data ? '<img src="https://img.icons8.com/material/48/000000/ok--v1.png">' : '<img src="https://img.icons8.com/color/48/000000/cancel--v1.png">'
                                },
                                targets: 8
                            },
                            {
                                render: function (data, type, row) {
                                    return data ? data : '<img src="https://img.icons8.com/android/24/000000/minus.png">'
                                },
                                targets: 9
                            },
                            {
                                render: function (data, type, row) {
                                    return data ? data : '<img src="https://img.icons8.com/android/24/000000/minus.png">'
                                },
                                targets: 11
                            }
                        ]
                    });

                    $('#animalTable tbody').on('click', 'tr', function (data) {
                        var idata = data;
                        return function () {
                            console.log(idata[this.rowIndex - 1]);
                            setAnimalData(idata[this.rowIndex - 1]);
                            enableContent('animal')
                        };
                    }(data_obj));

                    return data_obj;
                }
            })
        ;
    })
    ;

</script>
<script src="../vendor/script.js" type="text/javascript"></script>

</body>

</html>
