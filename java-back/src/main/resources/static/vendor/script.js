$('#datetimepicker11').datetimepicker({
    inline: true,
    sideBySide: true
});
$('#datetimepicker12').datetimepicker({
    inline: true,
    sideBySide: true
});

// $('#datetimepicker11').data("DateTimePicker").date()


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function getIntervalStr(interval) {
    return interval['id'] + ': from ' + interval['startDateTime'] + ' to ' + interval['endDateTime']
}

function arrayRemove(arr, value) {
    return arr.filter(function(ele){
        return ele != value;
    });
 }

function getDeleteInterval(id) {
    return function () {
        console.log("Deleting" + id)
        // intervals = arrayRemove(intervals, id)
        // intervals.pop()
        intervals.splice(id, 1)
        fillList(intervals)
    }
}

var intervals = []
maxid = 0
function fillList(intervals) {
    intervals_list.innerHTML = ''
    for(let i = 0; i < intervals.length; i++) {
        var p = document.createElement("li");
        p.onclick = 
        p.innerHTML = '<a href="">' + getIntervalStr(intervals[i]) +  '</a>'
        intervals_list.appendChild(p);
        var b = document.createElement("button");
        b.innerText = 'x'
        b.onclick = getDeleteInterval(i)
        p.appendChild(b);
    }   
}

function addInterval() {
    var moment1 = $('#datetimepicker11').data("DateTimePicker").date()
    var moment2 = $('#datetimepicker12').data("DateTimePicker").date()
    // moment2 = moment(moment2, 'MM-DD-YYYY HH:DD')
    console.log(moment1, moment2)
    var interval = {
        "id": maxid++,
        "startDateTime": moment1.format(),
        "endDateTime": moment2.format(),
    }
    intervals.push(interval)
    fillList(intervals)
}

// TODO: DEBUG, delete It
// addInterval()
// addInterval()
// addInterval()

selectedInterval = undefined
function editInterval(id) {

}