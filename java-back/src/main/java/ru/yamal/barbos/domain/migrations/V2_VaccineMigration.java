package ru.yamal.barbos.domain.migrations;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.yamal.barbos.domain.model.VaccineEntity;
import ru.yamal.barbos.domain.repository.VaccineRepository;

@Transactional
@RequiredArgsConstructor
@Component
public class V2_VaccineMigration implements Migration {

    private final VaccineRepository vaccineRepository;

    @Override
    public void migrate() {
        vaccineRepository.save(new VaccineEntity("Multikan-6", ""));
        vaccineRepository.save(new VaccineEntity("Multikan-8", ""));
        vaccineRepository.save(new VaccineEntity("Multifel-4", ""));
        vaccineRepository.save(new VaccineEntity("Multifel-6", ""));
        vaccineRepository.save(new VaccineEntity("Nobivak", ""));
        vaccineRepository.save(new VaccineEntity("Eurik", ""));
    }
}
