package ru.yamal.barbos.domain.migrations;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.yamal.barbos.domain.model.IntervalEntity;
import ru.yamal.barbos.domain.model.UserEntity;
import ru.yamal.barbos.domain.repository.IntervalRepository;
import ru.yamal.barbos.domain.repository.UserRepository;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Transactional
@Component
public class V6_ScheduleMigration implements Migration {

    private final IntervalRepository intervalRepository;
    private final UserRepository userRepository;

    @Override
    public void migrate() {
        UserEntity userEntity = userRepository.findAll().get(0);
        userEntity.setIsScheduleActive(true);
        UserEntity savedUser = userRepository.save(userEntity);

        IntervalEntity i1 = new IntervalEntity(LocalDateTime.now().minusMonths(2), LocalDateTime.now(), savedUser);
        intervalRepository.save(i1);
        IntervalEntity i2 = new IntervalEntity(LocalDateTime.now(), LocalDateTime.now().plusDays(2), savedUser);
        intervalRepository.save(i2);
    }
}
