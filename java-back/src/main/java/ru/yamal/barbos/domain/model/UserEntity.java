package ru.yamal.barbos.domain.model;

import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hibernate.envers.RelationTargetAuditMode.NOT_AUDITED;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true, exclude = {"schedule"})
@ToString(exclude = {"schedule"})
@Entity
public class UserEntity extends BaseEntity<Long> {

    @Column(unique = true, nullable = false)
    private String phoneNumber;

    private String password;

    @ElementCollection(fetch = FetchType.EAGER)
    private List<Role> roles;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String surname;

    @Column
    private String email;

    @Column
    private Boolean isScheduleActive = false;

    @Audited(targetAuditMode = NOT_AUDITED)
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Set<IntervalEntity> schedule = new HashSet<>();

}
