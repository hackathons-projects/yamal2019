package ru.yamal.barbos.domain.model;

import lombok.*;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Audited
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
@Entity
public class IntervalEntity extends BaseEntity<Long> {

    @Column
    private LocalDateTime startDateTime;
    @Column
    private LocalDateTime endDateTime;
    @ManyToOne
    private UserEntity user;
}
