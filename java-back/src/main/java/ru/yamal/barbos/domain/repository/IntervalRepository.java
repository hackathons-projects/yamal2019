package ru.yamal.barbos.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yamal.barbos.domain.model.IntervalEntity;

import java.util.Optional;

public interface IntervalRepository extends JpaRepository<IntervalEntity, Long> {

    Optional<IntervalEntity> findById(Long id);
}
