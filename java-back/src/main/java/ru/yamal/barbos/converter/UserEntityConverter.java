package ru.yamal.barbos.converter;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.yamal.barbos.domain.model.UserEntity;
import ru.yamal.barbos.dto.IntervalDto;
import ru.yamal.barbos.dto.UserDto;

import java.util.stream.Collectors;

@Transactional
@Component
public class UserEntityConverter implements Converter<UserEntity, UserDto> {

    private final ModelMapper modelMapper;

    @Autowired
    public UserEntityConverter(@Lazy ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public UserDto convert(MappingContext<UserEntity, UserDto> context) {
        return new UserDto(
                context.getSource().getId(),
                context.getSource().getPhoneNumber(),
                context.getSource().getRoles(),
                context.getSource().getFirstName(),
                context.getSource().getLastName(),
                context.getSource().getSurname(),
                context.getSource().getIsScheduleActive(),
                context.getSource().getEmail(),
                context.getSource().getSchedule().stream().map(intervalEntity -> modelMapper.map(intervalEntity, IntervalDto.class)).collect(Collectors.toList())
        );
    }
}
