package ru.yamal.barbos.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.yamal.barbos.dto.IntervalDto;
import ru.yamal.barbos.service.IntervalService;

@RequiredArgsConstructor
@RequestMapping("/intervals")
@RestController
public class IntervalController {

    private final IntervalService intervalService;

    @GetMapping("/{id}")
    public IntervalDto getInterval(@PathVariable("id") Long id) {
        return intervalService.getInterval(id);
    }

    @DeleteMapping("/{id}")
    public IntervalDto deleteInterval(@PathVariable("id") Long id) {
        return intervalService.deleteInterval(id);
    }

    @PostMapping
    public IntervalDto create(@RequestBody IntervalDto intervalDto) {
        return intervalService.create(intervalDto);
    }
}
