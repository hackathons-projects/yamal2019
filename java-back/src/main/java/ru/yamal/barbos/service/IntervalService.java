package ru.yamal.barbos.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.yamal.barbos.domain.model.IntervalEntity;
import ru.yamal.barbos.domain.repository.IntervalRepository;
import ru.yamal.barbos.dto.IntervalDto;
import ru.yamal.barbos.exception.CustomException;

@Transactional
@RequiredArgsConstructor
@Service
public class IntervalService {

    private final IntervalRepository intervalRepository;
    private final ModelMapper modelMapper;

    public IntervalDto getInterval(Long id) {
        return modelMapper.map(intervalRepository.findById(id).orElseThrow(() -> new CustomException("Not found", HttpStatus.NOT_FOUND)), IntervalDto.class);
    }

    public IntervalDto deleteInterval(Long id) {
        IntervalEntity intervalEntity = intervalRepository.findById(id).orElseThrow(() -> new CustomException("Not found", HttpStatus.NOT_FOUND));
        intervalRepository.delete(id);
        return modelMapper.map(intervalEntity, IntervalDto.class);
    }

    public IntervalDto create(IntervalDto intervalDto) {
        IntervalEntity map = modelMapper.map(intervalDto, IntervalEntity.class);
        IntervalEntity saved = intervalRepository.save(map);
        return modelMapper.map(saved, IntervalDto.class);
    }

}
