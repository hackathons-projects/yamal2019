from collections import defaultdict


states = {
    0: 'FIRST_CONTACT',
    1: 'REGISTERED',
    2: 'AUTHORIZED',
    # TODO: Расширить диалог до еще одного уровня и покрыть бОльшую часть api java-back
    3: 'IDLE',  # Состояние ожидания ответа в диалоге
    31: 'GET_ANIMALS',  # вывести список всех животных в приюте
    32: 'ADD_ANIMAL',  # добавить новое животное в список
    33: 'UPDATE',  # изменить карточку животного
    34: 'APPEND_DESCR',  # добавить описание к животному

    42: 'MEM_TEST',  # узнать, кокое жывотное вы кусаити
    4: 'ERROR',
    5: 'PSHL_NH',
}

class EasyState:
    def __init__(self):
        self.states = defaultdict(lambda: 0)
        self.contexts = defaultdict(dict)
        self.vk = None
        from .utils import RegisterHandler, AuthorityHandler, GreetingHandler, WaitCommandHandler, \
            ErrorHandler, GetAnimalsHandler, AddAnimalHandler, UpdateAnimalHandler, \
            AppendDescrHandler, WaitDialogHandler, MainDialogHandler, StupidAnswer
        self.dispatch = {
            0: RegisterHandler,
            1: AuthorityHandler,
            # 2: GreetingHandler,
            2: MainDialogHandler,
            3: WaitCommandHandler,
            30: WaitDialogHandler,
            31: GetAnimalsHandler,
            32: AddAnimalHandler,
            33: UpdateAnimalHandler,
            34: AppendDescrHandler,
            4: ErrorHandler,
            123: StupidAnswer,
            # 6: MainDialogHandler,
            # 70: MainDialogWaitHandler,

        }
        
    def __setitem__(self, user, state):
        print('State ', self.states[user], '-> ', state)
        self.states[user] = state
    
    def __getitem__(self, user):
        return self.states[user]
    
    def context(self, user, context_upd):
        self.contexts[user].update(context_upd)
    
    def recieve(self, user, event):
        s = self.states[user]
        self.dispatch[s](self, user, event)
        
        if self[user] != 3:
            self.recieve(user, event)