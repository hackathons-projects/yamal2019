import json
import requests
from collections import defaultdict
import vk_api
from vk_api import VkUpload
from vk_api.utils import get_random_id
# from run import vk
# from src import *


JAVA_BACKEND_URL = "http://localhost:8080"


def WaitCommandHandler(machine, user, event: vk_api.bot_longpoll.VkBotMessageEvent=None):
    print('id{}: "{}"'.format(user, event.obj.text), end=' ')
    dispath = defaultdict(lambda : 3)
    dispath.update({
        1: 31,
        2: 32,
        3: 33,
        4: 5
    })
    try:
        machine[user] = dispath[int(event.obj.text)]
    except:
        machine[user] = 4


def ErrorHandler(machine, user, event: vk_api.bot_longpoll.VkBotMessageEvent=None):
    print('Something going wrong')
    machine.vk.messages.send(
        user_id=user,
        random_id=get_random_id(),
        message="Что-то сломалося, извините"
    )
    raise Exception()
    pass


def AddAnimalHandler():
    pass


def WaitDialogHandler():
    pass


def UpdateAnimalHandler():
    pass


def AppendDescrHandler():
    pass


def PNHHandler(machine, user, event: vk_api.bot_longpoll.VkBotMessageEvent=None):
    print('id{}: "{}"'.format(user, event.obj.text), end=' ')
    print('PNH')
    machine.vk.messages.send(
        user_id=user,
        random_id=get_random_id(),
        message="Иди нахуй, пидрила ты тупоебая!"
    )
    
    machine[user] = 3


def AuthorityHandler(machine, user, event: vk_api.bot_longpoll.VkBotMessageEvent=None):
#     user = "+70000000001"
    url = JAVA_BACKEND_URL + "/users/signin"
    print(url)
#     payload = "{\n\"phoneNumber\":\"+70000000001\",\n\"password\": \"001\"\n}"
    payload = json.dumps({"phoneNumber":user,"password": "string"})
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
        }

    response = requests.request("POST", url, data=payload, headers=headers)
    
    print(response)
    if response.status_code == 200:
        machine[user] = 2
        machine.context(user, {'token': response.text})
        return response.text
    else:
        machine[user] = 4
        try:
            r = json.loads(response.text)
            return r['status'], r['message']
        except Exception as e:
            return response.text


def RegisterHandler(machine, user, event: vk_api.bot_longpoll.VkBotMessageEvent=None):
    url = JAVA_BACKEND_URL + "/users/signup"
    print(url)
#     payload = "{\n\"phoneNumber\":\"+70000000001\",\n\"password\": \"001\"\n}"
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
    }
    
    payload = {
      "email": "sample@mail.com",
      "firstName": "string",
      "lastName": "string",
      "password": "string",
      "phoneNumber": user,
      "roles": [
        "ROLE_USER"
      ],
      "surname": "string"
    }
    payload = json.dumps(payload)
    
    response = requests.request("POST", url, data=payload, headers=headers)
    
    print(response)
    
    if response.status_code == 200:
        machine[user] = 2
        machine.context(user, {'token': response.text})
        return response.status_code, 
    else:
        machine[user] = 1
        try:
            r = json.loads(response.text)
            return r['status'], r['message']
        except Exception as e:
            return response.text


greetMsg = '''
Привет, это бот, выберете желаемое действие:

1 - вывести список всех животных в приюте
2 - добавить новое животное в список
3 - изменить карточку животного
4 - добавить описание к животному
..
..
42 - узнать, кокое жывотное вы кусаити
'''


def GreetingHandler(machine, user: str, event: vk_api.bot_longpoll.VkBotMessageEvent=None):
    print('id{}: "{}"'.format(user, event.obj.text), end=' ')
    print('Greeting')
    machine.vk.messages.send(
        user_id=user,
        random_id=get_random_id(),
        message=greetMsg
    )
    
    machine[user] = 3


def GetAnimalsHandler(machine, user, event=None):
    url = JAVA_BACKEND_URL + "/animals/"
    print(url)

    headers = {
        'authorization': "Bearer " + machine.contexts[user]['token'],
        'cache-control': "no-cache",
        'postman-token': "109c7b22-02b4-7142-964b-de9024dbfc1a"
        }

    response = requests.request("GET", url, headers=headers)
    machine[user] = 2
    print(response.text)

    machine.vk.messages.send(
        user_id=user,
        random_id=get_random_id(),
        message=response.text  # TODO: привести текст к читабельному виду
    )

    return response.text


main_dialog_msg = '''
Привет, это бот, выберете желаемое действие:

1 - Найдено бездомное животное
2 - Хочу найти питомца
3 - Проблемы с моим питомцем
4 - Хочу помочь приюту

'''
main_dialog_msg = '''
Вы можете помочь:
1. Деньгами
2. Есть вещи для приюта (корм, лекарства, стройматериалы, другое)
3. Работа 
4. Автопомощь
5. Передержка
6. Соцсети
7. Фотографирование животных
8. Погулять с животными

Ваши контакты:
Имя
Телефон
'''

for_admin_point = '5 - управление базой данных'


def MainDialogHandler(machine, user: str, event: vk_api.bot_longpoll.VkBotMessageEvent=None):
    print('id{}: "{}"'.format(user, event.obj.text), end=' ')
    print('Main dialog')
    machine.vk.messages.send(
        user_id=user,
        random_id=get_random_id(),
        message=main_dialog_msg
    )
    
    machine[user] = 123
    

def StupidAnswer(machine, user: str, event: vk_api.bot_longpoll.VkBotMessageEvent=None):
    print('Main dialog')
    machine.vk.messages.send(
        user_id=user,
        random_id=get_random_id(),
        message="Спасибо за то, что откликнулись, ваши данные переданы волонтерам"
    )
    machine[user] = 3


def MainDialogWaitHandler(machine, user, event: vk_api.bot_longpoll.VkBotMessageEvent=None):
    print('id{}: "{}"'.format(user, event.obj.text), end=' ')
    dispath = defaultdict(lambda : 3)
    dispath.update({
        1: 71,
        2: 72,
        3: 73,
        4: 74,
        5: 75
    })
    try:
        machine[user] = dispath[int(event.obj.text)]
    except:
        machine[user] = 4


find_msg = '''
Спасибо, что не остались равнодушными, пожалуйста ответьте на несколько вопросов:
Местоположение животного, кошка это или собака, здоровое оно или больное
контакты волонтера - +7-999-123-5665
'''
def MainFindHandler(machine, user, event: vk_api.bot_longpoll.VkBotMessageEvent=None):
    machine.vk.messages.send(
        user_id=user,
        random_id=get_random_id(),
        message=find_msg
    )
    
    machine[user] = 710


find_out_msg = '''
Спасибо за помощь, если Вам не сложно, то можете остаться с животным ненадолго, пока к нему не приедет помощь.
'''
def FindOutHandler(machine, user: str, event: vk_api.bot_longpoll.VkBotMessageEvent=None):
    print('id{}: "{}"'.format(user, event.obj.text), end=' ')
    print('Main dialog')
    machine.vk.messages.send(
        user_id=user,
        random_id=get_random_id(),
        message=find_out_msg
    )
    
    machine[user] = 3


want_take_msg = '''
Здавствуйте, спасибо, что заинтересовались возможностью взять животного их приюта
Наши животные уже привиты и стерилизованы. 
Заводя себе питомца, помните, что это не только приятное времяпрепровождение, но и ответственность, а также необходимость ухода за ним.
'''
def TakeAnimaGreet(machine, user, event: vk_api.bot_longpoll.VkBotMessageEvent=None):
    machine.vk.messages.send(
        user_id=user,
        random_id=get_random_id(),
        message=want_take_msg
    )
    machine[user] = 80

show_take_dialog_msg = '''
Наши питомцы:
'''
show_take_msg_end = '''
1 - предыдущие 5
2 - следующие 5
3 - выборать понравившееся животное
4 - выход
'''
def ShowTakeAnimals(machine, user, event: vk_api.bot_longpoll.VkBotMessageEvent=None):
    animals = None  # get animals

    machine.vk.messages.send(
        user_id=user,
        random_id=get_random_id(),
        message=show_take_dialog_msg
    )

    print('id{}: "{}"'.format(user, event.obj.text), end=' ')
    dispath = defaultdict(lambda : 3)
    dispath.update({
        1: 81,
        2: 82,
        3: 83,
        4: 3,
    })
    try:
        machine[user] = dispath[int(event.obj.text)]
    except:
        machine[user] = 4


def ShowIncrement():
    pass


def ShowDecrement():
    pass


animal_choosen_msg = '''
Спасибо, что выбрали его, мы с вами свяжемся
'''
def AnimalChoosen():
    pass


guide_greet_msg = '''
Мы не специализируемся на помощи животным, у которых есть хозяин, но возможно сможем помочь с ответом на ваши вопросы
1 - Медицинская помощь
2 - Передержка на время отсутствия
3 - Сдача животного в приют
'''
def GuideGreeting():
    pass


disease_guide_msg = '''
Мы не оказываем услуги профессиональной медицинской помощи, также мы не работаем с другими животными, кроме кошек и собак, контакты ветклиники: +7-123-456-7890
Ветстанции - +7-123-456-7890
Если нет возможности обратиться к ветеринару, а травма легкая, то волонтер может помочь с перевязкой или первой помощью, но не более того
'''
def GuideDiseaseAns():
    pass


nurse_msg = '''
Извините, но мы не оказываем услуг по передержки животных
Контакты гостиницы для животных: +7-123-456-7890
'''
def NurseAns():
    pass


# TODO: не знаю что здесь написать
drop_msg = '''
Опишите в одном посте причину и краткое описание питомца
'''
def DropMsgGreeteng():
    pass


drop_ans_msg = '''
'''
def DropAns():
    pass


take_part_msg = '''
'''
def TakePartGreeteing():
    pass


def TakePartDispatcher():
    pass


volonteer_msg = '''
Спасибо за помощь, контакты руководителя - +7-123-456-7890
'''
def TakePartOut():
    pass