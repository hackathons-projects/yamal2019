# -*- coding: utf-8 -*-
import vk_api
from vk_api.bot_longpoll import VkBotLongPoll, VkBotEventType

from vk_api import VkUpload
from vk_api.utils import get_random_id

import sys
import traceback
import src
from multiprocessing import Process, Queue
from src.utils import *
from src.state_machine import EasyState


vk = None
machine  = None
def main():
    global vk
    global machine

    machine = EasyState()

    vk_session = vk_api.VkApi(token='6857d8fca104b8e625ccd6b25603ed190ae2abe2c94c6e6da67afd0731ba7eb2fa26d83f328c6579cee2c')

    vk = vk_session.get_api()
    machine.vk = vk

    upload = VkUpload(vk_session)  # Для загрузки изображений

    longpoll = VkBotLongPoll(vk_session, '187852299')

    for event in longpoll.listen():
        if event.type == VkBotEventType.MESSAGE_NEW and event.object.text:
            machine.recieve(event.obj.from_id, event)
            # GreetingHandler(machine, event.obj.from_id, event=event)


if __name__ == "__main__":
    # TODO: Superviser for bot
    
    while True:
        try:
            # bot_daemon = Process(target=main, args=())
            # bot_daemon.daemon = True
            # bot_daemon.start()
            # bot_daemon.join()
            main()
        except Exception as e:
            exc_info = sys.exc_info()
            traceback.print_exception(*exc_info)
            del exc_info
            # break
